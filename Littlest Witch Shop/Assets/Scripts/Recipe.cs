using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Recipe : ScriptableObject
{
    [SerializeField] private Material baseMat;
    [SerializeField] private int baseAmount;
    [SerializeField] private Material reagent1;
    [SerializeField] private int reagent1Amount;
    [SerializeField] private Material reagent2;
    [SerializeField] private int reagent2Amount;
    [SerializeField] private CraftedItem result;
}
