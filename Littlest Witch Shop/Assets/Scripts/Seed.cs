using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "New Seed", menuName = "Items/Seed")]
public class Seed : ScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private int buyPrice;
    [SerializeField] private string description;
    [SerializeField] private ItemType itemCat;

    public string Name { get { return name; } }
    public string Description { get { return description; } set { description = value; } }
    public int BuyPrice { get { return buyPrice; } }
    public ItemType ItemType { get { return itemCat; } }


    public string ColouredName
    {
        get
        {
            string hexColour = ColorUtility.ToHtmlStringRGB(itemCat.TextColour);
            return $"<color=#{hexColour}>{Name}</color>";
        }
    }

    public string GetTooltipInfo()
    {
        StringBuilder builder = new StringBuilder();

        builder.Append(ItemType.Name).AppendLine();
        builder.Append("Hire Price: ").Append(buyPrice).Append(" Gold").AppendLine();
        builder.Append("Ability: ").Append(Description).AppendLine();

        return builder.ToString();
    }
}
