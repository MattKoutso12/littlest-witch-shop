using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CrewCard : MonoBehaviour
{
    [SerializeField] Crew crew;
    [SerializeField] UI_CrewInventory crewInvent;
    [SerializeField] List<Crew> crews;
    [SerializeField] TextMeshProUGUI abilityText;
    [SerializeField] List<string> abilities;
    private int blueImpPrice = 40;

    private int randInt;
    private void Awake()
    {
        SetAbilityText();
    }

    public void HireCrew()
    {
        foreach (Crew listedCrew in crews)
        {
            if(listedCrew.Name == crew.Name)
            {
                crewInvent.inventory.AddCrew(crew);
            }
        }
        PersistantManager.Instance.RemoveGold(blueImpPrice);
    }

    private void SetAbilityText()
    {
        int randInt = Random.Range(0, 10);
        crew.Ability = abilities[randInt];
        abilityText.text = "Ability: " + abilities[randInt];
    }
}
