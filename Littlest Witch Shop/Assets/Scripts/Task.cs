using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "New Order", menuName = "Items/Orders")]
public class Task : ScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] public int orderLength;
    [SerializeField] private int reward;
    [SerializeField] public List<Item> products;
    [SerializeField] private ItemType itemCat;

    public string Name { get { return name; } }

    public int Reward { get { return reward; } }

    public int OrderLength { get { return orderLength; } }

    public ItemType ItemType { get { return itemCat; } }

    public string ColouredName
    {
        get
        {
            string hexColour = ColorUtility.ToHtmlStringRGB(itemCat.TextColour);
            return $"<color=#{hexColour}>{Name}</color>";
        }
    }
    public string GetTooltipInfo()
    {
        StringBuilder builder = new StringBuilder();

        //builder.Append(ItemType.Name).AppendLine();

        builder.Append("Requires:").AppendLine();

        foreach(Item listedItem in products)
        {
            builder.Append(listedItem.Name).AppendLine();
        }

        builder.Append("<color=green>").Append(OrderLength).Append(" Days Left").Append("</color>").AppendLine();
        builder.Append("Sell Price: ").Append(Reward).Append(" Gold").AppendLine();

        return builder.ToString();
    }
}
