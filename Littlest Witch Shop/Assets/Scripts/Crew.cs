using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "New Crew", menuName = "Items/Crewmembers")]
public class Crew : ScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private int hirePrice;
    [SerializeField] private string ability;
    [SerializeField] private ItemType itemCat;
    public enum Reference
    {
        Imp,
        Witch,
        Faerie,
        Dwarf,
        BlueImp,
    }
    private ICrewHolder crewHolder;

    public string Name { get { return name; } }

    public string Ability { get { return ability; } set { ability = value; } }
    public int HirePrice { get { return hirePrice; } }

    public ItemType ItemType { get { return itemCat; } }

    public string ColouredName
    {
        get
        {
            string hexColour = ColorUtility.ToHtmlStringRGB(itemCat.TextColour);
            return $"<color=#{hexColour}>{Name}</color>";
        }
    }
    public string GetTooltipInfo()
    {
        StringBuilder builder = new StringBuilder();

        builder.Append(ItemType.Name).AppendLine();
        builder.Append("Hire Price: ").Append(HirePrice).Append(" Gold").AppendLine();
        builder.Append("Ability: ").Append(Ability).AppendLine();

        return builder.ToString();
    }

    public Sprite GetSprite(string name)
    {
        switch (name)
        {
            default:
            case "Imp": return ItemAssets.Instance.s_Imp;
            case "Faerie": return ItemAssets.Instance.s_Fairy;
            case "Dwarf": return ItemAssets.Instance.s_Dwarf;
            case "Blue Imp": return ItemAssets.Instance.s_BlueImp;
        }
    }
    public void SetCrewHolder(ICrewHolder crewHolder)
    {
        this.crewHolder = crewHolder;
    }

    public ICrewHolder GetItemHolder()
    {
        return crewHolder;
    }

    public void RemoveFromItemHolder()
    {
        if (crewHolder != null)
        {
            // Remove from current Item Holder
            crewHolder.RemoveCrew(this);
        }
    }

    public void MoveToAnotherItemHolder(ICrewHolder newItemHolder)
    {
        RemoveFromItemHolder();
        // Add to new Item Holder
        newItemHolder.AddCrew(this);
    }
}
