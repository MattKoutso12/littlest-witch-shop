﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OrderButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] public Task order;

    public void OnPointerEnter(PointerEventData eventData)
    {
        PersistantManager.Instance.ShowTooltip(order);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        PersistantManager.Instance.HideTooltip();
    }

    private void Update()
    {
        if (order.OrderLength == 0)
        {
            Destroy(this.gameObject);
        }
    }

    

}
