﻿using System.Collections;
using UnityEngine;

public interface IItemHolder
{
    void RemoveItem(InventoryItem item);
    void AddItem(InventoryItem item);
    bool CanAddItem();
}