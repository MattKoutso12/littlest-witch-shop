﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{

    [SerializeField] private Player player;
    [SerializeField] private UI_Invetory uiInventory;
    [SerializeField] private UI_CrewInventory crewInvent;

    [SerializeField] private UI_CraftingSystem uiCraftingSystem;
    [SerializeField] private UI_PlantingSystem uiPlantingSystem;
    [SerializeField] private UI_PlantingSystem uiPlantingSystem2;
    [SerializeField] private UI_PlantingSystem uiPlantingSystem3;

    private void Start()
    {
        uiInventory.SetPlayer(player);
        uiInventory.SetInventory(player.GetInventory());
        crewInvent.SetPlayer(player);
        crewInvent.SetInventory(player.GetCrewInventory());
        CraftingSystem craftingSystem = new CraftingSystem();
        PlantingSystem plantingSystem = new PlantingSystem();
        uiPlantingSystem.SetPlantingSystem(plantingSystem);
        uiCraftingSystem.SetCraftingSystem(craftingSystem);
        PlantingSystem plantingSystem2 = new PlantingSystem();
        uiPlantingSystem2.SetPlantingSystem(plantingSystem2);
        PlantingSystem plantingSystem3 = new PlantingSystem();
        uiPlantingSystem3.SetPlantingSystem(plantingSystem3);
    }
}
