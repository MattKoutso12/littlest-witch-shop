using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour
{
    [SerializeField] UI_CraftingSystem craftingTester;
    [SerializeField] UI_Invetory testing;
    [SerializeField] List<Item> items;
    private void OnMouseDown()
    {
        if(craftingTester.craftingSystem.GetOutputItem() != null)
        {
            InventoryItem item = craftingTester.craftingSystem.GetOutputItem();
            testing.inventory.AddItem(item);
            craftingTester.craftingSystem.RemoveItem(item);
        }
    }
}
