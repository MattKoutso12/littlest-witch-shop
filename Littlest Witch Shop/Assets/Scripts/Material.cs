﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "New Material", menuName = "Items/Materials")]
public class Material : Item
{
    [SerializeField] private ItemType itemCat;
    [SerializeField] private string useText = "Use: Something";

    public ItemType ItemType { get { return itemCat; } }
    public override string ColouredName
    {
        get 
        {
            string hexColour = ColorUtility.ToHtmlStringRGB(itemCat.TextColour);
            return $"<color=#{hexColour}>{Name}</color>";
        }
    }

    public override string GetTooltipInfo()
    {
        StringBuilder builder = new StringBuilder();

        builder.Append(ItemType.Name).AppendLine();
        builder.Append("<color=green>Use: ").Append(useText).Append("</color>").AppendLine();
        builder.Append("Sell Price: ").Append(SellPrice).Append(" Gold").AppendLine();

        return builder.ToString();
    }
}