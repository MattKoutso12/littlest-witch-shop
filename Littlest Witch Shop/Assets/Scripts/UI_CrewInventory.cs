﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_CrewInventory : MonoBehaviour
{

    [SerializeField] private Transform pfUI_Crew;
    public CrewInventory inventory;
    private Transform crewSlotContainer;
    private Transform crewSlotTemplate;
    private Player player;
    private void Awake()
    {
        crewSlotContainer = transform.Find("CrewSlotContainer");
        crewSlotTemplate = crewSlotContainer.Find("CrewSlotTemplate");
        crewSlotTemplate.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

    public void SetPlayer(Player player)
    {
        this.player = player;
    }

    public void SetInventory(CrewInventory inventory)
    {
        this.inventory = inventory;

        inventory.OnItemListChanged += Inventory_OnItemListChanged;

        RefreshInventoryItems();
    }

    private void Inventory_OnItemListChanged(object sender, System.EventArgs e)
    {
        RefreshInventoryItems();
    }

    private void RefreshInventoryItems()
    {
        foreach (Transform child in crewSlotContainer)
        {
            if (child == crewSlotTemplate) continue;
            Destroy(child.gameObject);
        }

        int x = 0;
        int y = 0;
        float itemSlotCellSize = 54f;
        foreach (CrewInventory.InventorySlot inventorySlot in inventory.GetInventorySlotArray())
        {
            Crew crew = inventorySlot.GetItem();

            RectTransform itemSlotRectTransform = Instantiate(crewSlotTemplate, crewSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);

            itemSlotRectTransform.anchoredPosition = new Vector2(x * itemSlotCellSize, -y * itemSlotCellSize);

            if (!inventorySlot.IsEmpty())
            {
                // Not Empty, has Item
                Transform uiItemTransform = Instantiate(pfUI_Crew, crewSlotContainer);
                uiItemTransform.GetComponent<RectTransform>().anchoredPosition = itemSlotRectTransform.anchoredPosition;
                CrewDragDrop uiItem = uiItemTransform.GetComponent<CrewDragDrop>();
                uiItem.SetItem(crew);
            }

            CrewInventory.InventorySlot tmpInventorySlot = inventorySlot;

            UI_CrewSlot uiItemSlot = itemSlotRectTransform.GetComponent<UI_CrewSlot>();
            uiItemSlot.SetOnDropAction(() => {
                // Dropped on this UI Item Slot
                Crew draggedItem = UI_CrewDrag.Instance.GetItem();
                draggedItem.RemoveFromItemHolder();
                inventory.AddItem(draggedItem, tmpInventorySlot);
            });

            x++;
            int itemRowMax = 7;
            if (x >= itemRowMax)
            {
                x = 0;
                y++;
            }
        }
    }
}