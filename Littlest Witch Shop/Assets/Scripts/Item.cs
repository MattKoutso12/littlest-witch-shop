using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : ScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private int sellPrice;
    [SerializeField] public InventoryItem.ItemType itemType;

    public string Name { get { return name; } }
    public int SellPrice { get { return sellPrice; } }

    public abstract string ColouredName { get; }
    public abstract string GetTooltipInfo();
}
