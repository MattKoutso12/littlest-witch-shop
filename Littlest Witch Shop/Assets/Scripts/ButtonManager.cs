using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] UI_CraftingSystem craftingTester;
    [SerializeField] UI_PlantingSystem plantSystem;
    [SerializeField] UI_PlantingSystem plantSystem2;
    [SerializeField] UI_PlantingSystem plantSystem3;
    [SerializeField] UI_Invetory testing;
    [SerializeField] UI_CrewInventory crew;

    [SerializeField] List<GameObject> recipes;
    [SerializeField] GameObject hireScreen;
    [SerializeField] Button hireScreenToggler;
    [SerializeField] GameObject buySeedScreen;
    [SerializeField] Button seedScreenToggler;
    [SerializeField] GameObject upgradeScreen;
    [SerializeField] Button upgradeToggler;
    [SerializeField] GameObject huntingScreen;
    [SerializeField] Button huntingToggler;

    [SerializeField] public GameObject recipe;

    public void Sleep()
    {
        if (craftingTester.craftingSystem.GetOutputItem() != null)
        {
            InventoryItem item = craftingTester.craftingSystem.GetOutputItem();
            testing.inventory.AddItemMergeAmount(item);
            craftingTester.craftingSystem.RemoveItem(item);
        }
        if(plantSystem.plantingSystem.GetOutputItem() != null)
        {
            InventoryItem item = plantSystem.plantingSystem.GetOutputItem();
            testing.inventory.AddItemMergeAmount(item);
            plantSystem.plantingSystem.RemoveItem(item);
        }
        PersistantManager.Instance.increaseDay();
    }

    public void ToggleCrafting()
    {
        PersistantManager.Instance.craftingVisible = !PersistantManager.Instance.craftingVisible;
        craftingTester.gameObject.SetActive(PersistantManager.Instance.craftingVisible);
    }

    public void TogglePlanting()
    {
        PersistantManager.Instance.plantingVisible = !PersistantManager.Instance.plantingVisible;
        plantSystem.gameObject.SetActive(PersistantManager.Instance.plantingVisible);
    }

    public void TogglePlanting1()
    {
        PersistantManager.Instance.plantingVisible2 = !PersistantManager.Instance.plantingVisible2;
        plantSystem2.gameObject.SetActive(PersistantManager.Instance.plantingVisible2);
    }
    public void TogglePlanting2()
    {
        PersistantManager.Instance.plantingVisible3 = !PersistantManager.Instance.plantingVisible3;
        plantSystem3.gameObject.SetActive(PersistantManager.Instance.plantingVisible3);
    }
    public void ToggleCrew()
    {
        PersistantManager.Instance.crewVisible = !PersistantManager.Instance.crewVisible;
        crew.gameObject.SetActive(PersistantManager.Instance.crewVisible);
    }

    public void ToggleInventory()
    {
        PersistantManager.Instance.inventoryVisible = !PersistantManager.Instance.inventoryVisible;
        testing.gameObject.SetActive(PersistantManager.Instance.inventoryVisible);
    }
    public void ToggleRecipes()
    {
        PersistantManager.Instance.recipesVisible = !PersistantManager.Instance.recipesVisible;
        recipe.gameObject.SetActive(PersistantManager.Instance.recipesVisible);
    }

    public void ToggleHiringOn()
    {
        PersistantManager.Instance.hireScreenVisible = !PersistantManager.Instance.hireScreenVisible;
        hireScreen.gameObject.SetActive(PersistantManager.Instance.hireScreenVisible);

        hireScreenToggler.gameObject.SetActive(!PersistantManager.Instance.hireScreenVisible);
        Time.timeScale = 0;
    }
    public void ToggleHiringOff()
    {
        PersistantManager.Instance.hireScreenVisible = !PersistantManager.Instance.hireScreenVisible;
        hireScreen.gameObject.SetActive(PersistantManager.Instance.hireScreenVisible);

        hireScreenToggler.gameObject.SetActive(!PersistantManager.Instance.hireScreenVisible);
        Time.timeScale = 1;
    }

    public void ToggleBuyingSeedOn()
    {
        PersistantManager.Instance.seedScreenVisible = !PersistantManager.Instance.seedScreenVisible;
        buySeedScreen.gameObject.SetActive(PersistantManager.Instance.seedScreenVisible);

        seedScreenToggler.gameObject.SetActive(!PersistantManager.Instance.seedScreenVisible);
        Time.timeScale = 0;
    }

    public void ToggleBuyingSeedOff()
    {
        PersistantManager.Instance.seedScreenVisible = !PersistantManager.Instance.seedScreenVisible;
        buySeedScreen.gameObject.SetActive(PersistantManager.Instance.seedScreenVisible);

        seedScreenToggler.gameObject.SetActive(!PersistantManager.Instance.seedScreenVisible);
        Time.timeScale = 1;
    }

    public void ToggleUpgradeOn()
    {
        PersistantManager.Instance.upgradeScreenVisible = !PersistantManager.Instance.upgradeScreenVisible;
        upgradeScreen.gameObject.SetActive(PersistantManager.Instance.upgradeScreenVisible);

        upgradeToggler.gameObject.SetActive(!PersistantManager.Instance.upgradeScreenVisible);
        Time.timeScale = 0;
    }

    public void ToggleUpgradeOff()
    {
        PersistantManager.Instance.upgradeScreenVisible = !PersistantManager.Instance.upgradeScreenVisible;
        upgradeScreen.gameObject.SetActive(PersistantManager.Instance.upgradeScreenVisible);

        upgradeToggler.gameObject.SetActive(!PersistantManager.Instance.upgradeScreenVisible);
        Time.timeScale = 1;
    }

    public void TogggleHuntingOn()
    {
        PersistantManager.Instance.huntingScreenVisible = !PersistantManager.Instance.huntingScreenVisible;
        huntingScreen.gameObject.SetActive(PersistantManager.Instance.huntingScreenVisible);

        huntingToggler.gameObject.SetActive(!PersistantManager.Instance.huntingScreenVisible);
        Time.timeScale = 0;
    }

    public void ToggleHuntingOff()
    {
        PersistantManager.Instance.huntingScreenVisible = !PersistantManager.Instance.huntingScreenVisible;
        huntingScreen.gameObject.SetActive(PersistantManager.Instance.huntingScreenVisible);

        huntingToggler.gameObject.SetActive(!PersistantManager.Instance.huntingScreenVisible);
        Time.timeScale = 1;
    }
}
