using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class SeedCard : MonoBehaviour
{
    [SerializeField] Seed seed;
    [SerializeField] GameObject fake;
    [SerializeField] TextMeshProUGUI abilityText;
    [SerializeField] List<Seed> seeds;

    private void Awake()
    {
        SetText();
    }

    public void BuySeed()
    {
        foreach (Seed listedSeed in seeds)
        {
            if (listedSeed.Name == seed.Name)
            {
                fake.SetActive(true);

            }
        }
    }

    private void SetText()
    {
    
    }
}