﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "New Crafted Item", menuName = "Items/CraftedItem")]
public class CraftedItem : Item
{
    [SerializeField] private ItemType itemCat;
    [SerializeField] private string[] ingred = { };

    public ItemType ItemType { get { return itemCat; } }
    public override string ColouredName
    {
        get 
        {
            string hexColour = ColorUtility.ToHtmlStringRGB(itemCat.TextColour);
            return $"<color=#{hexColour}>{Name}</color>";
        }
    }

    public override string GetTooltipInfo()
    {
        StringBuilder builder = new StringBuilder();

        builder.Append(ItemType.Name).AppendLine();
        builder.Append("<color=blue>Ingredients: ").AppendLine();
        int i = 0;
        foreach (string ingredient in ingred)
        {
            builder.Append(ingredient);
            if(ingred[i] == ingred[ingred.Length - 1])
            {
                builder.Append("</color>").AppendLine();
            } else if(ingred[i] != ingred[ingred.Length - 1])
            {
                builder.AppendLine();
            }
            i++;
        }
        builder.Append("Sell Price: ").Append(SellPrice).Append(" Gold").AppendLine();

        return builder.ToString();
    }
}