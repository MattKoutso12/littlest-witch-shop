using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_CraftingSystem : MonoBehaviour
{
    [SerializeField] private Transform pfUI_Item;
    private Transform[] slotTransformArray;
    private Transform outputSlotTransform;
    private Transform crewSlotTransform;
    private Transform itemContainer;
    private Transform crewContainer;
    public CraftingSystem craftingSystem;
    [SerializeField] private Transform pfUI_Crew;
    [SerializeField] private Transform pfUI_Output;

    private void Awake()
    {
        Transform gridContainer = transform.Find("Grid Container");
        itemContainer = transform.Find("Item Container");

        slotTransformArray = new Transform[CraftingSystem.GRID_SIZE];

        for (int i = 0; i < CraftingSystem.GRID_SIZE; i++)
        {
            slotTransformArray[i] = gridContainer.Find("grid" + i);
            UI_CraftingItemSlot craftingItemSlot = slotTransformArray[i].GetComponent<UI_CraftingItemSlot>();
            craftingItemSlot.SetX(i);
            craftingItemSlot.OnItemDropped += UI_CraftingSystem_OnItemDropped;
        }
        crewSlotTransform = transform.Find("Crew Slot");
        crewContainer = transform.Find("Crew Container");
        UI_CrewSlot crewSlot = crewSlotTransform.GetComponent<UI_CrewSlot>();
        crewSlot.SetX(0);
        crewSlot.OnItemDropped += UI_CraftingSystem_OnCrewDropped;

        outputSlotTransform = transform.Find("OutputSlot");
        this.gameObject.SetActive(false);
    }

    public void SetCraftingSystem(CraftingSystem craftingSystem)
    {
        this.craftingSystem = craftingSystem;
        craftingSystem.OnGridChanged += CraftingSystem_OnGridChanged;
        UpdateVisual();
    }

    private void CraftingSystem_OnGridChanged(object sender, EventArgs e)
    {
        UpdateVisual();
    }

    private void UpdateVisual()
    {
        foreach (Transform child in itemContainer)
        {
            Destroy(child.gameObject);
        }
        foreach (Transform child in crewContainer)
        {
            Destroy(child.gameObject);
        }
        for (int i = 0; i < CraftingSystem.GRID_SIZE; i++)
        {
            if (!craftingSystem.IsEmpty(i))
            {
                    CreateItem(i, craftingSystem.GetItem(i));
            }
        }
        if (!craftingSystem.IsCrewEmpty())
        {
            CreateCrew(craftingSystem.GetCrew());
        }
        if (craftingSystem.GetOutputItem() != null)
        {
            CreateItemOutput(craftingSystem.GetOutputItem());
        }
    }

    private void UI_CraftingSystem_OnItemDropped(object sender, UI_CraftingItemSlot.OnItemDroppedEventArgs e)
    {
        craftingSystem.TryAddItem(e.item, e.x);
    }
    public void UI_CraftingSystem_OnCrewDropped(object sender, UI_CrewSlot.OnItemDroppedEventArgs e)
    {
        craftingSystem.TryAddCrew(e.worker);
    }

    private void CreateItem(int x, InventoryItem item)
    {
        Transform itemTransform = Instantiate(pfUI_Item, itemContainer);
        RectTransform itemRectTransform = itemTransform.GetComponent<RectTransform>();
        itemRectTransform.anchoredPosition = slotTransformArray[x].GetComponent<RectTransform>().anchoredPosition;
        itemTransform.GetComponent<DragDrop>().SetItem(item);
    }
    private void CreateCrew(Crew crew)
    {
        Transform crewTransform = Instantiate(pfUI_Crew, crewContainer);
        RectTransform itemRectTransform = crewTransform.GetComponent<RectTransform>();
        itemRectTransform.anchoredPosition = crewSlotTransform.GetComponent<RectTransform>().anchoredPosition;
        crewTransform.GetComponent<CrewDragDrop>().SetItem(crew);
    }
    private void CreateItemOutput(InventoryItem item)
    {
        Transform itemTransform = Instantiate(pfUI_Output, itemContainer);
        RectTransform itemRectTransform = itemTransform.GetComponent<RectTransform>();
        itemRectTransform.anchoredPosition = outputSlotTransform.GetComponent<RectTransform>().anchoredPosition;
        itemTransform.localScale = Vector3.one * 1.5f;
        itemTransform.GetComponent<OutputSetter>().SetItem(item);
    }
}
