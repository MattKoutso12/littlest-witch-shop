using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UI_CrewSlot: MonoBehaviour, IDropHandler
{
    public event EventHandler<OnItemDroppedEventArgs> OnItemDropped;
    public Action onDropAction;
    public class OnItemDroppedEventArgs : EventArgs
    {
        public Crew worker;
        public int x;
    }

    private int x;
    public void SetOnDropAction(Action onDropAction)
    {
        this.onDropAction = onDropAction;
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("Freddy's Coming for you");
        UI_CrewDrag.Instance.Hide();
        Crew worker = UI_CrewDrag.Instance.GetItem();
        OnItemDropped?.Invoke(this, new OnItemDroppedEventArgs { worker = worker, x = x });
        if(onDropAction != null)
        {
            onDropAction();
        }
    }

    public void SetX(int x)
    {
        this.x = x;
    }
}
