﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu]
public class ItemType : ScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private Color textColour;

    public string Name { get { return name; } }

    public Color TextColour { get { return textColour; } }

}