﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrewInventory : ICrewHolder
{
    public event EventHandler OnItemListChanged;

    public List<Crew> crewList;
    private Action<Crew> useItemAction;
    public InventorySlot[] inventorySlotArray;

    public CrewInventory(Action<Crew> useItemAction, int inventorySlotCount)
    {
        this.useItemAction = useItemAction;
        crewList = new List<Crew>();

        inventorySlotArray = new InventorySlot[inventorySlotCount];
        for (int i = 0; i < inventorySlotCount; i++)
        {
            inventorySlotArray[i] = new InventorySlot(i);
        }
    }

    public InventorySlot GetEmptyInventorySlot()
    {
        foreach (InventorySlot inventorySlot in inventorySlotArray)
        {
            if (inventorySlot.IsEmpty())
            {
                return inventorySlot;
            }
        }
        Debug.LogError("Cannot find an empty InventorySlot!");
        return null;
    }

    public InventorySlot GetInventorySlotWithItem(Crew crew)
    {
        foreach (InventorySlot inventorySlot in inventorySlotArray)
        {
            if (inventorySlot.GetItem() == crew)
            {
                return inventorySlot;
            }
        }
        Debug.LogError("Cannot find Crew " + crew + " in a InventorySlot!");
        return null;
    }

    public void AddCrew(Crew crew)
    {
        crewList.Add(crew);
        crew.SetCrewHolder(this);
        GetEmptyInventorySlot().SetItem(crew);
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void RemoveCrew(Crew crew)
    {
        GetInventorySlotWithItem(crew).RemoveItem();
        crewList.Remove(crew);
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void AddItem(Crew crew, InventorySlot inventorySlot)
    {
        // Add Item to a specific Inventory slot
        crewList.Add(crew);
        crew.SetCrewHolder(this);
        inventorySlot.SetItem(crew);

        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void UseItem(Crew crew)
    {
        useItemAction(crew);
    }

    public List<Crew> GetItemList()
    {
        return crewList;
    }

    public InventorySlot[] GetInventorySlotArray()
    {
        return inventorySlotArray;
    }

    public bool CanAddCrew()
    {
        return GetEmptyInventorySlot() != null;
    }

    /*
     * Represents a single Inventory Slot
     * */
    public class InventorySlot
    {

        private int index;
        private Crew crew;

        public InventorySlot(int index)
        {
            this.index = index;
        }

        public Crew GetItem()
        {
            return crew;
        }

        public void SetItem(Crew crew)
        {
            this.crew = crew;
        }

        public void RemoveItem()
        {
            crew = null;
        }

        public bool IsEmpty()
        {
            return crew == null;
        }

    }

}