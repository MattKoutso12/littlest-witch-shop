using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class PersistantManager : MonoBehaviour
{
    public static PersistantManager Instance { get; private set; }

    [SerializeField] private TooltipPopup tooltipPopup;
    [SerializeField] private List<Item> items;
    [SerializeField] private List<Task> orders;
    [SerializeField] private List<Crew> crews;

    [SerializeField] private int goldAmount;
    [SerializeField] private TextMeshProUGUI goldText;
    [SerializeField] public int dayCount;
    [SerializeField] private TextMeshProUGUI dayText;

    //all visibility toggles
    [SerializeField] public bool inventoryVisible;
    [SerializeField] public bool crewVisible;
    [SerializeField] public bool craftingVisible;
    [SerializeField] public bool plantingVisible;
    [SerializeField] public bool plantingVisible2;
    [SerializeField] public bool plantingVisible3;
    [SerializeField] public bool recipesVisible;
    [SerializeField] public bool hireScreenVisible;
    [SerializeField] public bool seedScreenVisible;
    [SerializeField] public bool upgradeScreenVisible;
    [SerializeField] public bool huntingScreenVisible;
    [SerializeField] public UI_Invetory invetory;

    public event EventHandler OnGoldChanged;
    public event EventHandler OnDayChanged;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        goldText.text = "" + goldAmount;
        dayText.text = "" + dayCount;
    }

    private void Start()
    {
        OnGoldChanged += _OnGoldChanged;
        OnDayChanged += _OnDayChanged;
    }

    private void _OnDayChanged(object sender, EventArgs e)
    {
        dayText.text = "" + dayCount;
        foreach(Task listedTask in orders)
        {
            listedTask.orderLength--;
        }
    }

    private void _OnGoldChanged(object sender, EventArgs e)
    {
        goldText.text = "" + goldAmount;
    }

    public void ShowTooltip(InventoryItem.ItemType itemType)
    {
        foreach(Item item in items)
        {
            if(item.itemType == itemType)
            {
                tooltipPopup.DisplayInfo(item);
            }
        }
    }

    public void ShowTooltip(Task order)
    {
        foreach (Task listedOrder in orders)
        {
            if (order.name == listedOrder.name)
            {
                tooltipPopup.DisplayInfo(listedOrder);
            }
        }
    }
    public void ShowTooltip(Crew crew)
    {
        foreach (Crew listedCrew in crews)
        {
            if (crew.name == listedCrew.name)
            {
                tooltipPopup.DisplayInfo(listedCrew);
            }
        }
    }

    public void HideTooltip()
    {
        tooltipPopup.HideInfo();
    }

    public void RemoveGold(int x)
    {
        goldAmount -= x;
        OnGoldChanged?.Invoke(this, EventArgs.Empty);
    }

    public void AddGold(int x)
    {
        goldAmount += x;
        OnGoldChanged?.Invoke(this, EventArgs.Empty);
    }

    public void increaseDay()
    {
        dayCount++;
        OnDayChanged?.Invoke(this, EventArgs.Empty);
    }
}
