using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class CrewDragDrop : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{

    private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    private Image image;
    private Crew worker;
    
    [SerializeField] private List<Crew> crews;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GetComponentInParent<Canvas>();
        image = transform.Find("CrewImage").GetComponent<Image>();
    }

    public void SetItemButton()
    {
        foreach (Crew listedItem in crews)
        {
            if (listedItem.Name == worker.Name)
            {
                CrewButton itemButton = GetComponent<CrewButton>();
                itemButton.crew = listedItem;
            }
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = .5f;
        canvasGroup.blocksRaycasts = false;
        UI_CrewDrag.Instance.Show(worker);
    }

    public void OnDrag(PointerEventData eventData)
    {

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        UI_CrewDrag.Instance.Hide();
    }

    public void SetSprite(Sprite newSprite)
    {
        image.sprite = newSprite;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void SetItem(Crew worker)
    {
        this.worker = worker;
        SetSprite(worker.GetSprite(worker.Name));
        SetItemButton();
    }
}