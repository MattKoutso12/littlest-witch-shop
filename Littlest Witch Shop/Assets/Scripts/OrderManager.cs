using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderManager : MonoBehaviour
{
    [SerializeField] Task order;
    [SerializeField] UI_Invetory invetory;
    [SerializeField] GameObject fake;

    public void Fulfill()
    {/*
        foreach(InventoryItem item in invetory.inventory.GetItemList())
        {
            foreach(Item listeditem in order.products)
            {
                if(item.itemType == listeditem.itemType)
                {
                    invetory.inventory.RemoveItemAmount(listeditem.itemType, 1);
                    
                }
            }
        }
     */
        invetory.inventory.RemoveItemAmount(InventoryItem.ItemType.PoWB, 1);
        invetory.inventory.RemoveItemAmount(InventoryItem.ItemType.FaerieDust, 1);
        fake.SetActive(false);
        PersistantManager.Instance.AddGold(order.Reward);
    }
}
