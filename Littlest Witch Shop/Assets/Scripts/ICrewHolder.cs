﻿using System.Collections;
using UnityEngine;

public interface ICrewHolder
{
    void RemoveCrew(Crew crew);
    void AddCrew(Crew crew);
    bool CanAddCrew();
}