using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] public Item item;

    public void OnPointerEnter(PointerEventData eventData)
    {
        PersistantManager.Instance.ShowTooltip(item.itemType);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        PersistantManager.Instance.HideTooltip();
    }
}

