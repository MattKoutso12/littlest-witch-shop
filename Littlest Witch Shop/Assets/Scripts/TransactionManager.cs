using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransactionManager : MonoBehaviour
{
    [SerializeField] public InventoryItem.ItemType type;
    [SerializeField] public UI_Invetory invetory;
    [SerializeField] List<Item> items;
    private int buyPrice = 5;
    private int buyPrice2 = 10;

    //all upgrade prices and objects

    private int planterPrice = 50;
    private int craftingTablePrice = 15;
    private int waterSpoutPrice = 15;
    [SerializeField] public GameObject craftingTableImage;
    [SerializeField] public GameObject planterImage;

    public void BuyMoonleafSeed()
    {
        invetory.inventory.AddItemMergeAmount(new InventoryItem { itemType = type, amount = 1 });
        foreach (Item item in items)
        {
            if (item.itemType == type)
            {
                //PersistantManager.Instance.RemoveGold(item.SellPrice);
                PersistantManager.Instance.RemoveGold(buyPrice);
            }
        }
    }

    public void BuyWormrootSeed()
    {
        invetory.inventory.AddItemMergeAmount(new InventoryItem { itemType = type, amount = 1 });
        foreach (Item item in items)
        {
            if (item.itemType == type)
            {
                //PersistantManager.Instance.RemoveGold(item.SellPrice);
                PersistantManager.Instance.RemoveGold(buyPrice);
            }
        }
    }

    public void BuyMiskweedSeed()
    {
        invetory.inventory.AddItemMergeAmount(new InventoryItem { itemType = type, amount = 1 });
        foreach (Item item in items)
        {
            if (item.itemType == type)
            {
                //PersistantManager.Instance.RemoveGold(item.SellPrice);
                PersistantManager.Instance.RemoveGold(buyPrice2);
            }
        }
    }

    public void BuyPlanter()
    {
        planterImage.SetActive(true);
        PersistantManager.Instance.RemoveGold(planterPrice);
    }

    public void BuyCraftingTable()
    {
        craftingTableImage.SetActive(true);
        PersistantManager.Instance.RemoveGold(craftingTablePrice);
    }

    public void BuyWaterSpout()
    {
        PersistantManager.Instance.RemoveGold(waterSpoutPrice);
    }




}
