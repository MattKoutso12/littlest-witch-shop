using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public static Player Instance { get; private set; }

    private Inventory inventory;
    private CrewInventory crewInvent;
    
    private UI_CraftingSystem uiCraftingSystem;

    [SerializeField] float speed;
    
    private bool begin;
    [SerializeField] bool helpVisible;

    [SerializeField] GameObject help;

    private void Awake()
    {
        Instance = this;
        inventory = new Inventory(UseItem, 14);
        crewInvent = new CrewInventory(UseCrew, 14);
        begin = true;
    }

    public void UseItem(InventoryItem inventoryItem)
    {
        Debug.Log("Use Item: " + inventoryItem);
    }

    public void UseCrew(Crew crew)
    {
        Debug.Log("Crew:  " + crew);
    }

    private void Update()
    {
        float moveX = 0f;
        float moveY = 0f;
        if (Input.GetKey(KeyCode.W))
        {
            moveY = +1f;
        }
        if (Input.GetKey(KeyCode.S))
        {
            moveY = -1f;
        }
        if (Input.GetKey(KeyCode.A))
        {
            moveX = -1f;
        }
        if (Input.GetKey(KeyCode.D))
        {
            moveX = +1f;
        }
        Vector3 moveDir = new Vector3(moveX, moveY).normalized;
        transform.position += moveDir * speed * Time.deltaTime;

        if(begin)
        {
            help.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                help.SetActive(false);
                begin = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && !begin)
        {
            helpVisible = !helpVisible;
            help.SetActive(helpVisible);
        }
    }

    public Inventory GetInventory()
    {
        return inventory;
    }

    public CrewInventory GetCrewInventory()
    {
        return crewInvent;
    }

}
