using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantingSystem : IItemHolder, ICrewHolder
{
    public const int GRID_SIZE = 2;
    public event EventHandler OnGridChanged;
    private InventoryItem[] itemArray;
    private Crew crew;
    private Dictionary<InventoryItem.ItemType, InventoryItem.ItemType[]> plantDictionary;
    private InventoryItem output;

    public PlantingSystem()
    {
        itemArray = new InventoryItem[GRID_SIZE];

        plantDictionary = new Dictionary<InventoryItem.ItemType, InventoryItem.ItemType[]>();

        InventoryItem.ItemType[] MoonleafRecipe = new InventoryItem.ItemType[GRID_SIZE];
        MoonleafRecipe[0] = InventoryItem.ItemType.MoonleafSeed;
        MoonleafRecipe[1] = InventoryItem.ItemType.Water;
        plantDictionary[InventoryItem.ItemType.Moonleaf] = MoonleafRecipe;

        InventoryItem.ItemType[] WormrootRecipe = new InventoryItem.ItemType[GRID_SIZE];
        MoonleafRecipe[0] = InventoryItem.ItemType.WormrootSeed;
        MoonleafRecipe[1] = InventoryItem.ItemType.Water;
        plantDictionary[InventoryItem.ItemType.Wormroot] = WormrootRecipe;

        InventoryItem.ItemType[] PoppyRecipe = new InventoryItem.ItemType[GRID_SIZE];
        MoonleafRecipe[0] = InventoryItem.ItemType.PoppySeed;
        MoonleafRecipe[1] = InventoryItem.ItemType.Water;
        plantDictionary[InventoryItem.ItemType.Poppy] = PoppyRecipe;

    }

    public bool IsEmpty(int x)
    {
        return itemArray[x] == null;
    }
    public bool IsCrewEmpty()
    {
        return crew == null;
    }

    public InventoryItem GetItem(int x)
    {
        return itemArray[x];
    }

    public void SetItem(InventoryItem item, int x)
    {
        if (item != null)
        {
            item.RemoveFromItemHolder();
            item.SetItemHolder(this);
        }
        itemArray[x] = item;
        CreateOutput();
        OnGridChanged?.Invoke(this, EventArgs.Empty);
    }
    public void SetCrew(Crew crew)
    {
        if (crew != null)
        {
            crew.RemoveFromItemHolder();
            crew.SetCrewHolder(this);
        }
        this.crew = crew;
        CreateOutput();
        OnGridChanged?.Invoke(this, EventArgs.Empty);
    }
    private void IncreaseItemAmount(int x)
    {
        GetItem(x).amount++;
        OnGridChanged?.Invoke(this, EventArgs.Empty);
    }
    private void DecreaseItemAmount(int x)
    {
        if (GetItem(x) != null)
        {
            GetItem(x).amount--;
            if (GetItem(x).amount == 0)
            {
                RemoveItem(x);
            }
            OnGridChanged?.Invoke(this, EventArgs.Empty);
        }
    }
    private void RemoveItem(int x)
    {
        SetItem(null, x);
    }
    public Crew GetCrew()
    {
        return crew;
    }
    public bool TryAddItem(InventoryItem item, int x)
    {
        if (IsEmpty(x))
        {
            SetItem(item, x);
            return true;
        }
        else
        {
            if (item.itemType == GetItem(x).itemType)
            {
                IncreaseItemAmount(x);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public bool TryAddCrew(Crew crew)
    {
        if (IsCrewEmpty())
        {
            SetCrew(crew);
            return true;
        }
        else
        {
            if (crew.Name == GetCrew().Name)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public void RemoveItem(InventoryItem item)
    {
        if (item == output)
        {
            ConsumeRecipeItems();
            CreateOutput();
            OnGridChanged?.Invoke(this, EventArgs.Empty);
        }
        else
        {
            for (int x = 0; x < GRID_SIZE; x++)
            {
                if (GetItem(x) == item)
                {
                    RemoveItem(x);
                }
            }
        }
    }

    public void AddItem(InventoryItem item)
    {
        throw new System.NotImplementedException();
    }

    public bool CanAddItem()
    {
        return false;
    }

    private InventoryItem.ItemType GetRecipeOutput()
    {
        foreach (InventoryItem.ItemType recipeItem in plantDictionary.Keys)
        {
            InventoryItem.ItemType[] recipe = plantDictionary[recipeItem];

            bool completeRecipe = true;
            for (int x = 0; x < GRID_SIZE; x++)
            {
                if (recipe[x] != InventoryItem.ItemType.None)
                {
                    if (IsEmpty(x) || GetItem(x).itemType != recipe[x])
                    {
                        completeRecipe = false;
                    }
                }
            }

            if (completeRecipe)
            {
                return recipeItem;
            }
        }
        return InventoryItem.ItemType.None;
    }

    private void CreateOutput()
    {
        if(!IsCrewEmpty())
        {
            InventoryItem.ItemType recipeOutput = GetRecipeOutput();
            if (recipeOutput == InventoryItem.ItemType.None)
            {
                output = null;
            }
            else
            {
                output = new InventoryItem { itemType = recipeOutput, amount = 1 };
                output.SetItemHolder(this);
            }
        }
    }

    public InventoryItem GetOutputItem()
    {
        return output;
    }

    public void ConsumeRecipeItems()
    {
        for (int x = 0; x < GRID_SIZE; x++)
        {
            DecreaseItemAmount(x);
        }
    }

    public void RemoveCrew(Crew crew)
    {
        SetCrew(null);
    }

    public void AddCrew(Crew crew)
    {
        throw new NotImplementedException();
    }

    public bool CanAddCrew()
    {
        return false;
    }
}
