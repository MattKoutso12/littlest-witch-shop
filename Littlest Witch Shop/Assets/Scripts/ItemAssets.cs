using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAssets : MonoBehaviour
{

    public static ItemAssets Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }


    public Sprite s_Miskweed;
    public Sprite s_Wormroot;
    public Sprite s_Moonleaf;
    public Sprite s_Poppy;
    public Sprite s_ImpLeather;
    public Sprite s_Wool;
    public Sprite s_GoliathFinger;
    public Sprite s_SpiderEye;
    public Sprite s_QuartzGeode;
    public Sprite s_RedAmber;
    public Sprite s_BlackOpal;
    public Sprite s_Malachite;
    public Sprite s_RoS;
    public Sprite s_RoCP;
    public Sprite s_RoFP;
    public Sprite s_MuscleRelaxant;
    public Sprite s_FaerieDust;
    public Sprite s_MoTP;
    public Sprite s_PoNV;
    public Sprite s_PoCD;
    public Sprite s_PoWB;
    public Sprite s_Water;
    public Sprite s_WormrootSeed;
    public Sprite s_Imp;
    public Sprite s_Fairy;
    public Sprite s_Dwarf;
    public Sprite s_MoonleafSeed;
    public Sprite s_MiskweedSeed;
    public Sprite s_BlueImp;
    public Sprite s_PoppySeed;


}