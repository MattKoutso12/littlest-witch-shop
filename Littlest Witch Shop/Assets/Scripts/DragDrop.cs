using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{

    private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    private Image image;
    private InventoryItem item;
    private TextMeshProUGUI amountText;

    [SerializeField] private List<Item> items;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GetComponentInParent<Canvas>();
        image = transform.Find("Image").GetComponent<Image>();
        amountText = transform.Find("Amount Text").GetComponent<TextMeshProUGUI>();
    }

    public void SetItemButton()
    {
        foreach (Item listedItem in items)
        {
            if (listedItem.itemType == item.itemType)
            {
                ItemButton itemButton = GetComponent<ItemButton>();
                itemButton.item = listedItem;
            }
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = .5f;
        canvasGroup.blocksRaycasts = false;
        UI_ItemDrag.Instance.Show(item);
    }

    public void OnDrag(PointerEventData eventData)
    {
        //rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        UI_ItemDrag.Instance.Hide();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            // Right click, split
            if (item != null)
            {
                // Has item
                if (item.IsStackable())
                {
                    // Is Stackable
                    if (item.amount > 1)
                    {
                        // More than 1
                        if (item.GetItemHolder().CanAddItem())
                        {
                            // Can split
                            int splitAmount = Mathf.FloorToInt(item.amount / 2f);
                            item.amount -= splitAmount;
                            InventoryItem duplicateItem = new InventoryItem { itemType = item.itemType, amount = splitAmount };
                            item.GetItemHolder().AddItem(duplicateItem);
                        }
                    }
                }
            }
        }
    }

    public void SetSprite(Sprite newSprite)
    {
        image.sprite = newSprite;
    }

    public void SetAmountText(int amount)
    {
        if (amount <= 1)
        {
            amountText.text = "";
        }
        else
        {
            // More than 1
            amountText.text = amount.ToString();
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void SetItem(InventoryItem item)
    {
        this.item = item;
        SetSprite(InventoryItem.GetSprite(item.itemType));
        SetAmountText(item.amount);
        SetItemButton();
    }

}