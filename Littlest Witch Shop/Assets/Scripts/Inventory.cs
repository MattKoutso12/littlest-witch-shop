using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : IItemHolder
{

    public event EventHandler OnItemListChanged;

    public List<InventoryItem> itemList;
    private Action<InventoryItem> useItemAction;
    public InventorySlot[] inventorySlotArray;

    public Inventory(Action<InventoryItem> useItemAction, int inventorySlotCount)
    {
        this.useItemAction = useItemAction;
        itemList = new List<InventoryItem>();

        inventorySlotArray = new InventorySlot[inventorySlotCount];
        for (int i = 0; i < inventorySlotCount; i++)
        {
            inventorySlotArray[i] = new InventorySlot(i);
        }

        AddItem(new InventoryItem { itemType = InventoryItem.ItemType.Water, amount = 15 });
        AddItem(new InventoryItem { itemType = InventoryItem.ItemType.Poppy, amount = 2 });
        AddItem(new InventoryItem { itemType = InventoryItem.ItemType.Moonleaf, amount = 4 });
        AddItem(new InventoryItem { itemType = InventoryItem.ItemType.WormrootSeed, amount = 1 });
        AddItem(new InventoryItem { itemType = InventoryItem.ItemType.MoonleafSeed, amount = 1 });
        AddItem(new InventoryItem { itemType = InventoryItem.ItemType.PoWB, amount = 1 });
        AddItem(new InventoryItem { itemType = InventoryItem.ItemType.FaerieDust, amount = 1 });
        //AddItem(new InventoryItem { itemType = InventoryItem.ItemType.PoppySeed, amount = 1 });
    }

    public InventorySlot GetEmptyInventorySlot()
    {
        foreach (InventorySlot inventorySlot in inventorySlotArray)
        {
            if (inventorySlot.IsEmpty())
            {
                return inventorySlot;
            }
        }
        Debug.LogError("Cannot find an empty InventorySlot!");
        return null;
    }

    public InventorySlot GetInventorySlotWithItem(InventoryItem item)
    {
        foreach (InventorySlot inventorySlot in inventorySlotArray)
        {
            if (inventorySlot.GetItem() == item)
            {
                return inventorySlot;
            }
        }
        Debug.LogError("Cannot find Item " + item + " in a InventorySlot!");
        return null;
    }

    public void AddItem(InventoryItem item)
    {
        itemList.Add(item);
        item.SetItemHolder(this);
        GetEmptyInventorySlot().SetItem(item);
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void AddItemMergeAmount(InventoryItem item)
    {
        // Adds an Item and increases amount if same ItemType already present
        if (item.IsStackable())
        {
            bool itemAlreadyInInventory = false;
            foreach (InventoryItem inventoryItem in itemList)
            {
                if (inventoryItem.itemType == item.itemType)
                {
                    inventoryItem.amount += item.amount;
                    itemAlreadyInInventory = true;
                }
            }
            if (!itemAlreadyInInventory)
            {
                itemList.Add(item);
                item.SetItemHolder(this);
                GetEmptyInventorySlot().SetItem(item);
            }
        }
        else
        {
            itemList.Add(item);
            item.SetItemHolder(this);
            GetEmptyInventorySlot().SetItem(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void RemoveItem(InventoryItem item)
    {
        GetInventorySlotWithItem(item).RemoveItem();
        itemList.Remove(item);
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void RemoveItemAmount(InventoryItem.ItemType itemType, int amount)
    {
        RemoveItemRemoveAmount(new InventoryItem { itemType = itemType, amount = amount });
    }

    public void RemoveItemRemoveAmount(InventoryItem item)
    {
        // Removes item but tries to remove amount if possible
        if (item.IsStackable())
        {
            InventoryItem itemInInventory = null;
            foreach (InventoryItem inventoryItem in itemList)
            {
                if (inventoryItem.itemType == item.itemType)
                {
                    inventoryItem.amount -= item.amount;
                    itemInInventory = inventoryItem;
                }
            }
            if (itemInInventory != null && itemInInventory.amount <= 0)
            {
                GetInventorySlotWithItem(itemInInventory).RemoveItem();
                itemList.Remove(itemInInventory);
            }
        }
        else
        {
            GetInventorySlotWithItem(item).RemoveItem();
            itemList.Remove(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void AddItem(InventoryItem item, InventorySlot inventorySlot)
    {
        // Add Item to a specific Inventory slot
        itemList.Add(item);
        item.SetItemHolder(this);
        inventorySlot.SetItem(item);

        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void UseItem(InventoryItem item)
    {
        useItemAction(item);
    }

    public List<InventoryItem> GetItemList()
    {
        return itemList;
    }

    public InventorySlot[] GetInventorySlotArray()
    {
        return inventorySlotArray;
    }

    public bool CanAddItem()
    {
        return GetEmptyInventorySlot() != null;
    }

    /*
     * Represents a single Inventory Slot
     * */
    public class InventorySlot
    {

        private int index;
        private InventoryItem item;

        public InventorySlot(int index)
        {
            this.index = index;
        }

        public InventoryItem GetItem()
        {
            return item;
        }

        public void SetItem(InventoryItem item)
        {
            this.item = item;
        }

        public void RemoveItem()
        {
            item = null;
        }

        public bool IsEmpty()
        {
            return item == null;
        }

    }

}