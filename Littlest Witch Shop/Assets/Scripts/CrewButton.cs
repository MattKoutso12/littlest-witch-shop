using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CrewButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] public Crew crew;

    public void OnPointerEnter(PointerEventData eventData)
    {
        PersistantManager.Instance.ShowTooltip(crew);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        PersistantManager.Instance.HideTooltip();
    }
}
