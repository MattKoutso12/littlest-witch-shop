using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_PlantingSystem : MonoBehaviour
{
    [SerializeField] private Transform pfUI_Item;
    private Transform[] slotTransformArray;
    private Transform outputSlotTransform;
    private Transform crewSlotTransform;
    private Transform itemContainer;
    private Transform crewContainer;
    public PlantingSystem plantingSystem;
    [SerializeField] private Transform pfUI_Crew;
    [SerializeField] private Transform pfUI_Output;

    private void Awake()
    {
        Transform gridContainer = transform.Find("Grid Container");
        itemContainer = transform.Find("Item Container");

        slotTransformArray = new Transform[PlantingSystem.GRID_SIZE];

        for (int i = 0; i < PlantingSystem.GRID_SIZE; i++)
        {
            slotTransformArray[i] = gridContainer.Find("grid" + i);
            UI_PlantingItemSlot plantingItemSlot = slotTransformArray[i].GetComponent<UI_PlantingItemSlot>();
            plantingItemSlot.SetX(i);
            plantingItemSlot.OnItemDropped += UI_PlantingSystem_OnItemDropped;
        }

        crewSlotTransform = transform.Find("Crew Slot");
        crewContainer = transform.Find("Crew Container");
        UI_CrewSlot crewSlot = crewSlotTransform.GetComponent<UI_CrewSlot>();
        crewSlot.SetX(0);
        crewSlot.OnItemDropped += UI_PlantingSystem_OnCrewDropped;

        outputSlotTransform = transform.Find("OutputSlot");
        this.gameObject.SetActive(false);

    }

    public void SetPlantingSystem(PlantingSystem plantingSystem)
    {
        this.plantingSystem = plantingSystem;
        plantingSystem.OnGridChanged += PlantingSystem_OnGridChanged;
        UpdateVisual();
    }

    private void PlantingSystem_OnGridChanged(object sender, EventArgs e)
    {
        UpdateVisual();
    }

    private void UpdateVisual()
    {
        foreach (Transform child in itemContainer)
        {
            Destroy(child.gameObject);
        }
        foreach(Transform child in crewContainer)
        {
            Destroy(child.gameObject);
        }
        for (int i = 0; i < PlantingSystem.GRID_SIZE; i++)
        {
            if (!plantingSystem.IsEmpty(i))
            {
                CreateItem(i, plantingSystem.GetItem(i));
            }
        }
        if (!plantingSystem.IsCrewEmpty())
        {
            CreateCrew(plantingSystem.GetCrew());
        }
        if (plantingSystem.GetOutputItem() != null)
        {
            CreateItemOutput(plantingSystem.GetOutputItem());
        }
    }
    //
    private void UI_PlantingSystem_OnItemDropped(object sender, UI_PlantingItemSlot.OnItemDroppedEventArgs e)
    {
        plantingSystem.TryAddItem(e.item, e.x);
    }

    private void UI_PlantingSystem_OnCrewDropped(object sender, UI_CrewSlot.OnItemDroppedEventArgs e)
    {
        plantingSystem.TryAddCrew(e.worker);
    }

    private void CreateItem(int x, InventoryItem item)
    {
        Transform itemTransform = Instantiate(pfUI_Item, itemContainer);
        RectTransform itemRectTransform = itemTransform.GetComponent<RectTransform>();
        itemRectTransform.anchoredPosition = slotTransformArray[x].GetComponent<RectTransform>().anchoredPosition;
        itemTransform.GetComponent<DragDrop>().SetItem(item);
    }
    private void CreateCrew(Crew crew)
    {
        Transform crewTransform = Instantiate(pfUI_Crew, crewContainer);
        RectTransform itemRectTransform = crewTransform.GetComponent<RectTransform>();
        itemRectTransform.anchoredPosition = crewSlotTransform.GetComponent<RectTransform>().anchoredPosition;
        crewTransform.GetComponent<CrewDragDrop>().SetItem(crew);
    }
    private void CreateItemOutput(InventoryItem item)
    {
        Transform itemTransform = Instantiate(pfUI_Output, itemContainer);
        RectTransform itemRectTransform = itemTransform.GetComponent<RectTransform>();
        itemRectTransform.anchoredPosition = outputSlotTransform.GetComponent<RectTransform>().anchoredPosition;
        itemTransform.localScale = Vector3.one * 1.5f;
        itemTransform.GetComponent<OutputSetter>().SetItem(item);
    }
}